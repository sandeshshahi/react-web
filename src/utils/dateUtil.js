import moment from "moment";

export const formatDate = (date,format='YYYY-MM-DD')=>{
    if (!date) return;
    return moment(date).format(format);
}

export const formatTime = (time,format='hh:mm')=>{
    if (!time) return;
    return moment(time).format(format);
}