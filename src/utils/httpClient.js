import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BASE_URL,
    responseType: 'json',
    timeout: 30000,
    timeoutErrorMessage: 'Server took too long to respond'
})

const getHeaders = (isSecured) => {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecured) {
        options['Authorization'] = localStorage.getItem('token')
    }

    return options;
}

const GET = (url, isSecured = false, params = {}) => {
    return http.get(url, {
        headers: getHeaders(isSecured),
        params
    });
}

const POST = (url, data, isSecured = false, params = {}) => {
    return http.post(url, data, {
        headers: getHeaders(isSecured),
        params
    });
}

const PUT = (url, data, isSecured = false, params = {}) => {
    return http.put(url, data, {
        headers: getHeaders(isSecured),
        params
    });
}

const DELETE = (url, isSecured = false, params = {}) => {
    return http.delete(url, {
        headers: getHeaders(isSecured),
        params
    });
}

const UPLOAD = (method, url, data = {}, files = []) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        // append textual data in form data
        for (let key in data) {
            formData.append(key, data[key]);

        }
        // append files in form data
        files.forEach((file, index) => {
            formData.append('images', file, file.name)
        })
        xhr.onreadystatechange = () => {
            console.log('xhr.ready state >>', xhr.readyState);
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response)
                }
            }
        }

        xhr.open(method, `${BASE_URL}${url}?token=${localStorage.getItem('token')}`, true)
        xhr.send(formData);

    })


}

export const httpClient = {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}