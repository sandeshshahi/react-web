import React from 'react';
// import { Login } from './auth/login/login.component';
// import { Register } from './auth/register/register.component';
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


// functional component
// functional component must have return block
// it must return single html node
// return will contain jsx code
// 1st lesson to group jsx code we use parenthesis
// 2nd lesson {} interpolation
// {} is used to provide value inside JSX
export const App = (props) => {
    return (
        <div>
            {/* <Header isLoggedIn={false}></Header> */}
            <AppRouting />
            <ToastContainer></ToastContainer>
        </div >
    )


}






// root component
// this component will export content to be rendered in index.js


// component
// component is basic building block of react
// component is responsible for returning single html node 
// component can be written as class based as well as functional
// component can be statefull as well as stateless

// statefull component ==> component that maintains data within itself
// stateless component ==> component that doesnt maintains data within itself

// react version 17.02

// react 16.8 
// hooks are introduced from react 16.8
// functional component can be used as statefull component

// class based component for statefull
// functional component for stateless


// glossary
// props and state
// props (props bhaneko component ko lagi incomming data) 
// and state (state bhaneko component bhitra ko data manage garnu paryo bhane state chaiyo)


// component life cycle hook

// 3 phase of components life cycle
// init
// update
// destroy

// init ==> 
// constructor
// render(); browser ma kei kura dekhine render le garera ho
// componentDidMount(); component ek choti render bhaisakepachi run hune self invoked function
// data fetch,data prepare


// update phase
// either state change or props change
// render();
// componentDidUpdate; self invoked funciton
// componentDidUpdate(prevProps,prevState)

// destroy phase
// componentWillUnMount();
