import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import './header.component.css'

const logout = (history) => {
    // localstorage ko clearance
    // navigation
    localStorage.clear();
    history.push("/")
}
const HeaderComponent = (props) => {
    console.log('props in headers>>', props);
    const content = props.isLoggedIn
        ? <ul className="navList">
            <li className="navItem">
                <NavLink activeClassName="selected" to="/dashboard">Home</NavLink>
            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/about">About</NavLink>
            </li>

            <li className="navItem">
                <NavLink activeClassName="selected" to="/contact">Contact</NavLink>

            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/settings/admin">Settings</NavLink>

            </li>
            <li className="navItem">
                <button onClick={() => logout(props.history)} className="btn btn-info logout">Logout</button>

            </li>

        </ul>
        : <ul className="navList">
            <li className="navItem">
                <NavLink activeClassName="selected" to="/home">Home</NavLink>
            </li>
            <li className="navItem">
                <NavLink activeClassName="selected" to="/register">Register</NavLink>
            </li>
            <li className="navItem">
                <NavLink exact activeClassName="selected" to="/">Login</NavLink>
            </li>
        </ul >
    return (
    <div className="navBar">
        {content}
    </div>
)

}

export const Header = withRouter(HeaderComponent)