
import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import ForgotPassword from './auth/forgotPassword/forgotPassword.component';
import { Login } from './auth/login/login.component';
import { Register } from './auth/register/register.component';
import ResetPassword from './auth/resetPassword/resetPassword.component';
import { Header } from './common/header/header.component';
import { Sidebar } from './common/sidebar/sidebar.component';
import AddProduct from './products/addProduct/addProduct.component';
import { EditProduct } from './products/editProduct/editProduct.component';
import { SearchProduct } from './products/searchProdcut/searchProduct.component';
import { ViewProduct } from './products/viewProducts/viewProducts.component';
import Messages from './users/messages/message.component';

// BrowserRouter le ecosystem provided garcha overall routing configuration ko lagi
// route bhaneko chai configuration ie k auda k load garne bhanne kura haru

const Home = (props) => {
    console.log('props in home>>', props)
    return (
        <p>Home Page</p>
    )
}
const Dashboard = (props) => {
    console.log('props in Dashboard >>', props)
    const name = process.env.REACT_APP_APP_NAME;
    return (
        <div>
            <h2>Welcome to {name}</h2>
            <p>Please use side navigation menu or contact system administrator</p>
            <small>dashboard in progress</small>
        </div>
    )
}

// const About = (props) => {
//     return (
//         <p>About Page</p>
//     )
// }

// const Contact = (props) => {
//     return (
//         <p>Contact Page</p>
//     )
// }

// const Settings = (props) => {
//     console.log('props in setting>>', props)
//     return (
//         <p>Settings Page</p>
//     )
// }

const PageNotFound = (props) => {
    return (
        <div>
            <h2>Not Found</h2>
            <img src="./images/notfound.jpg" alt="not found image" width="600px"></img>

        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => {
            return localStorage.getItem('token')
                ? <div>
                    <Header isLoggedIn={true}></Header>
                    <Sidebar></Sidebar>
                    <div className="main-content">


                        <Component {...routeProps} ></Component>
                    </div>
                </div>
                : <Redirect to='/'></Redirect>
        }}></Route>
    )
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => {

            return (<div>
                <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
                <div className="main-content">

                    <Component {...routeProps} ></Component>
                </div>
            </div>
            )
        }}></Route>
    )
}

const AuthRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => {

            return (<div>
                <Header isLoggedIn={false}></Header>
                <div className="main-content">

                    <Component {...routeProps} ></Component>
                </div>
            </div>
            )
        }}></Route>
    )
}

export const AppRouting = (props) => {
    return (
        <BrowserRouter>
            {/* <Header isLoggedIn={true}></Header> */}
            <Switch>

                <AuthRoute path="/" exact component={Login}></AuthRoute>
                <AuthRoute path="/reset_password/:token" component={ResetPassword}></AuthRoute>
                <AuthRoute path="/forgot_password" component={ForgotPassword}></AuthRoute>
                <AuthRoute path="/register" component={Register}></AuthRoute>
                <PublicRoute path="/home" component={Home}></PublicRoute>
                <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                <ProtectedRoute path="/view_products" component={ViewProduct}></ProtectedRoute>
                <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                <ProtectedRoute path="/search_product" component={SearchProduct}></ProtectedRoute>
                <ProtectedRoute path="/messages" component={Messages}></ProtectedRoute>





                <PublicRoute component={PageNotFound}></PublicRoute>

            </Switch>


        </BrowserRouter>
    )
}

// summary
// routing ==> which page to load in which url
// library ==>  react-router-dom

// seperate component for routing setup
// <BrowserRouter is wrapper for routing configuration>
// Route is response for config path, component
// switch ==>  once we have path specific component it will not show the empty path component
// Link,NavLing ==> navigation through click event

// on every component specified by Route
// we have three props
// History ==> function to navigate
// Match ==> used to get dynamic values from url [part of url]
// location ==> for accessing optional parameters ? .... and data from navigation


