import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { formatDate } from '../../../utils/dateUtil'
import { handleError } from '../../../utils/error.handler'
import { httpClient } from '../../../utils/httpClient'
import { notify } from '../../../utils/notify'
import { Loader } from '../../loader/loader.component'
// import ReactTooltip from 'react-tooltip'

const IMG_URL = process.env.REACT_APP_IMG_URL;

export class ViewProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: false,
            products: []
        }
    }
    componentDidMount() {
        const { productData } = this.props;
        if (productData) {
            return this.setState({
                products: productData
            })
        }
        this.setState({
            isLoading: true
        })
        httpClient.GET('/product', true)
            .then(response => {
                this.setState({
                    products: response.data
                })

            })
            .catch(err => {
                handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })

    }

    removeProduct = (id, index) => {
        let confirmation = window.confirm('Are you sure to remove>')
        if (confirmation) {
            httpClient.DELETE(`/product/${id}`, true)
                .then(response => {
                    notify.showInfo('product removed')
                    const { products } = this.state;
                    products.splice(index, 1);
                    this.setState({
                        products
                    })

                })
                .catch(err => {
                    handleError(err);
                })
        }

    }

    editProduct = id => {
        this.props.history.push(`/edit_product/${id}`)
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Created At</th>
                        <th>Price</th>
                        <th>Tags</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        (this.state.products || []).map((product, index) => (
                            <tr key={product._id}>
                                <td>{index + 1}</td>
                                <td><Link to={`product_details/${product._id}`}>{product.name}</Link></td>
                                <td>{product.category}</td>
                                <td>{product.price}</td>
                                <td>{formatDate(product.createdAt, 'ddd  YYYY/MM/DD')}</td>
                                <td>{product.tags.toString()}</td>
                                <td>
                                    <img src={`${IMG_URL}/${product.images[0]}`} alt="product image" width="200px"></img>
                                </td>
                                <td>
                                    <button onClick={() => this.editProduct(product._id)} className="btn btn-default" title="Edit Product">
                                        <i style={{ color: "blue" }} className="fa fa-pencil"></i>
                                    </button>
                                    <button onClick={() => this.removeProduct(product._id, index)} className="btn btn-default" title="Delete Product">
                                        <i style={{ color: "red" }} className="fa fa-trash"></i>
                                    </button>

                                </td>
                            </tr>

                        ))
                    }


                </tbody>
            </table>

        return (
            <React.Fragment>
                <h2>View Products</h2>
                {this.props.productData && (
                    <button className="btn btn-info" onClick={() => this.props.resetSearch()}>search again</button>
                )}
                {content}

            </React.Fragment>
        )
    }
}
