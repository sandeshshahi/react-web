import React, { Component } from 'react';
import * as io from 'socket.io-client';
const socket_URL = process.env.REACT_APP_SOCKET_URL;

export default class Messages extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }
    componentDidMount() {
        this.socket = io(socket_URL)
        this.runSocket();

    }

    runSocket = () => {
        this.socket.on('hi', (data) => {
            console.log('at hi>>', data)
            this.socket.emit('hello', 'hello from client react application')
        })
    }

    render() {
        return (
            <React.Fragment>
                <h2>Messages</h2>
                <p>Let's Chat</p>


            </React.Fragment>
        )
    }
}
