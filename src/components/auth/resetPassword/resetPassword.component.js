import React, { Component } from 'react'
import { SubmitButton } from './../../common/submitButton/submitButton.component'
import { httpClient } from './../../../utils/httpClient'
import { handleError } from './../../../utils/error.handler'
import { notify } from '../../../utils/notify'
import { Link } from 'react-router-dom'


export default class ResetPassword extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: {
                password: '',
                confirmPassword: ''
            },
            error: {
                password: '',
                confirmPassword: ''

            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    componentDidMount() {
        this.token = this.props.match.params['token']
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            },

        }), () => {

            this.validateForm(name);
        })
    }

    validateForm = fieldName => {
        let errMsg;
        switch (fieldName) {

            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['confirmPassword']
                        ? this.state.data['confirmPassword'] === this.state.data[fieldName]
                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'

                    : 'required field'
                break;

            case 'confirmPassword':
                errMsg = this.state.data[fieldName]
                    ? this.state.data['password']
                        ? this.state.data['password'] === this.state.data[fieldName]

                            ? ''
                            : 'password did not match'
                        : this.state.data[fieldName].length > 6
                            ? ''
                            : 'weak password'
                    : 'required field'

                break;
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }

        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            })

        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { password } = this.state.data;
        httpClient.POST(`/auth/reset-password/${this.token}`, { password })
            .then(response => {
                notify.showInfo("password reset successful, please login")
                this.props.history.push('/');
            })
            .catch(err => {
                handleError(err)
                this.setState({
                    isSubmitting: false
                })
            })

    }

    render() {
        return (
            <div className="auth-box">
                <h2>Reset Password</h2>
                <p>Please choose your passord wisely</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" name="password" id="password" placeholder="Password" onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.password}</p>

                    <label htmlFor="confirmPassword">confirmPassword</label>
                    <input className="form-control" type="password" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange} ></input>
                    <p className="error">{this.state.error.confirmPassword}</p>

                    <hr></hr>
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >

                    </SubmitButton>
                </form>
                <hr />
                <p>back to <Link to="/">login</Link> </p>
            </div>
        )
    }
}
