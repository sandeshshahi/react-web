import React, { Component } from 'react';
import './login.component.css';
import { SubmitButton } from './../../common/submitButton/submitButton.component'
import { Link } from 'react-router-dom';
import { notify } from '../../../utils/notify';
import { handleError } from './../../../utils/error.handler'
import { httpClient } from './../../../utils/httpClient';
import { redirectToDashboard } from '../../../utils/util';
// import axios from "axios";
// const base_url = process.env.REACT_APP_BASE_URL;



// class based component
// RENDER METHOD IS COMPULSION IN CLASS BASED COMPONENT
// render method must have return block it should return single html node

const defaultForm = {
    username: '',
    password: '',
}
export class Login extends Component {

    constructor(props) {
        super(props); // parent constructor lai call gareko
        console.log('at first >>')
        this.state = {
            data: {
                ...defaultForm
            }, error: {
                ...defaultForm
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false,

        }
    }
    componentDidMount() {
        const isRemember = JSON.parse(localStorage.getItem('remember_me'));
        // console.log('is remember >>', typeof isRemember)
        if (isRemember) {
            this.props.history.push('/dashboard')
        }
        // console.log('props is >>', this.props)


        // console.log('at init >> componentDidMount ')
        // let i = 1;
        // this.abcd = setInterval(() => {
        //     i++;
        //     this.setState({
        //         chapter: i
        //     })

        // }, 1000);
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log('prev state chapter>>', prevState.chapter)
        // console.log('once updated or state change >>', this.state.chapter)
    }

    componentWillUnmount() {
        // console.log('once component is destroyed');
        // clearInterval(this.abcd)
    }

    // TODO Learn lifecycle method
    validateForm = () => {
        let usernameErr = false;
        let passwordErr = false;
        let validForm = true;

        if (!this.state.data.username) {
            usernameErr = 'required field';
            validForm = false;
        }
        if (!this.state.data.password) {
            passwordErr = 'required field';
            validForm = false;
        }
        this.setState({
            error: {
                username: usernameErr,
                password: passwordErr
            }

        })
        return validForm;
    }

    submit = (e) => {
        e.preventDefault();
        // notify.showSuccess('Login in Progress')

        const isValidForm = this.validateForm();
        if (!isValidForm) return;
        this.setState({
            isSubmitting: true
        })
        // API
        // we can use fetch for API call and http request pathauna sakchau
        // fetch({
        //     url: '',
        //     method: 'POST',
        //     body: this.state.data
        // }).then()

        // or we can use axios recommended
        // axios.post(`${base_url}/auth/login`, this.state.data, {
        //     headers: {
        //         'Content-Type': 'application/json'
        //     },
        //     params: {},
        //     responseType: 'json',
        //     timeout: 10000,
        //     timeoutErrorMessage: 'server unreachable'

        // })

        httpClient.POST('/auth/login', this.state.data)
            .then((response) => {
                console.log('response >>', response)// http response
                // http response
                // response.data ==> server response
                // web storage 
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('remember_me', this.state.remember_me)
                notify.showSuccess(`Welcome ${response.data.user.username}`)
                redirectToDashboard(this.props.history);

            })
            .catch(err => {
                handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })


    }
    change = (e) => {

        const { name, value, checked } = e.target;

        // console.log('name >>', name)
        // console.log('value >>', value)
        if (name === 'remember_me') {
            return this.setState({
                [name]: checked

            })
        }

        this.setState((prevState) => ({
            data: {
                ...prevState.data,
                [name]: value
            }
        }), () => {
            this.validateForm('change', name)
        })


    }

    render() {
        // console.log('render at second')
        // try to keep UI logic inside render but not in return

        return (
            <div className="auth-box">
                <h2>Login</h2>
                <p>Please login to continue</p>
                <form className="form-group" onSubmit={this.submit}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" name="username" placeholder="Username" id="username" onChange={this.change}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" id="password" placeholder="Password" onChange={this.change} name="password"></input>
                    <p className="error">{this.state.error.password}</p>
                    <input type="checkbox" name="remember_me" onChange={this.change}></input>
                    <label>&nbsp; Remember Me</label>
                    <hr />
                    <SubmitButton
                        isSubmitting={this.state.isSubmitting}
                        enabledLabel="login"
                        disabledLabel="loggin in.."
                    ></SubmitButton>


                </form>

                <hr />
                <p>Don't have an account?</p>
                <p className="float-left"><Link to="/register"> Register here</Link></p>
                <p className="float-right"><Link to="/forgot_password">forgot password</Link></p>
            </div >

        )
    }
}